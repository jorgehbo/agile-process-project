# Prerequisitos
- Instalar NodeJS: https://nodejs.org/en/ (https://nodejs.org/dist/v9.9.0/node-v9.9.0-x64.msi)
    * verificar la version de node y npm instalado
        - $ node -v
        - $ npm -v

- Instalar angular CLI:
    * $ npm install -g @angular/cli
    

- Entrar al directorio del proyecto y ejecutar lo siguientes comandos:
    - Instala las dependencias del proyecto
        * $ npm install
    
    - Inicia y levanta el proyecto generalmente en "http://localhost:4200/"
        * $ ng serve -o           

# AgileDemoV1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. 
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

