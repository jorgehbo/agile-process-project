import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import my components
import { HomeComponent } from './home/home.component';
import { RegistrarComponent } from './registrar/registrar.component';

const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'registrar', component: RegistrarComponent},
    {path: 'avisos', component: RegistrarComponent},
    {path: '', component: HomeComponent},
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];

// load the routes
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
