export class Aviso {
    constructor (
        public title: string,
        public language: string,
        public description: string
    ) {}
}
