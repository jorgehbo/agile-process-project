import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import our setting of routing
import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { AvisoComponent } from './aviso/aviso.component';

/* Start: Material Design Mudules*/
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
/* End: Material Design Mudules*/

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        RegistrarComponent,
        AvisoComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        routing, // routing
        BrowserAnimationsModule,

        /* Start: Material Design Mudules*/
        MatToolbarModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        MatInputModule,
        MatSelectModule
        /* End: Material Design Mudules*/
    ],
    providers: [appRoutingProviders],
    bootstrap: [AppComponent]
})
export class AppModule { }
