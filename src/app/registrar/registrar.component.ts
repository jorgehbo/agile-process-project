import { Component, OnInit } from '@angular/core';
import { Aviso } from '../aviso/aviso.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-registrar',
    templateUrl: './registrar.component.html',
    styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {
    public avisos: Array<Aviso> = [];
    public title = 'First demo Agile-app v1';
    public languages: any;
    public aviso: Aviso;
    public numAviso: Number = 0;

    constructor() { }

    ngOnInit(): void {
        this.languages = [
            {value: 'java', label: 'Java SE/EE'},
            {value: 'python', label: 'Python'},
            {value: 'angular', label: 'Angular 2+'},
            {value: 'csharp', label: 'C#'},
            {value: 'php', label: 'php'},
        ];
        this.aviso = new Aviso('', '', '');
        // tslint:disable-next-line:max-line-length
        this.avisos.push(new Aviso('New competition 20/April', 'PYTHON', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum'));
        /*
        this.avisos = new Array(4);
        for (let i = 0; i < this.avisos.length; i++) {
            this.avisos[i] = new Aviso('Title #' + i, 'JS', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum');
        }
        */
    }

    onSubmit(form: NgForm): void {
        console.log('Aviso entered: ' + this.aviso.title + ', ' + this.aviso.language + ', ' + this.aviso.description);
        this.avisos.push(new Aviso(this.aviso.title, this.aviso.language, this.aviso.description));
        form.resetForm();
    }
}
