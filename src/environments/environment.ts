// The file contents for the current environment will overwrite these during build.
// The build system defaults to the de environment which uses `environment.ts`, but if you do
// `ng build --en=prod` then `environment.prod.ts` will be used instead.
// The list of which en maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};
